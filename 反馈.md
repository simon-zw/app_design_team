- 小白没有预约功能了，因为我们最终使用的还是规定了小白的行驶路线，所有也就不可能去预约小白！
![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/235607_2d1a65ee_2233235.png "屏幕截图.png")
- 就是有的支付和更多车辆这总的，有按钮的就也做个个页面出来
- 也就预约小白这个功能没有，流程图这点也需要改变了
![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/235823_26527316_2233235.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0109/013000_793e2384_2233235.png "屏幕截图.png")
- 包车这里
![输入图片说明](https://images.gitee.com/uploads/images/2021/0109/000159_b3e54a82_2233235.png "屏幕截图.png")
- 可能要改成下面这种的界面更美观和可观
![输入图片说明](https://images.gitee.com/uploads/images/2021/0109/000826_d910eb11_2233235.png "屏幕截图.png")
- 这里要改成能筛选的机制和查询的
![输入图片说明](https://images.gitee.com/uploads/images/2021/0109/001307_3bfa7eab_2233235.png "屏幕截图.png")

- 目前大致就是这些问题啦！